################################################################
#     DEPLOY                                  
################################################################

FROM node:12.18.3-alpine as deploy
WORKDIR /app
RUN mkdir -p files/temp
COPY . .
RUN npm ci
EXPOSE 3000
CMD ["node", "."]
