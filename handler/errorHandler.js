const utils = require('loopback/lib/utils')

exports.createError = createError
exports.ErrorLogging = ErrorLogging

function createError(message, status, code) {
    let err = new Error(message)
    err.statusCode = status
    err.code = code
    return err
}

function ErrorLogging(message, status, code, cb) {
    cb = cb || utils.createPromiseCallback()
    let err = new Error(message)
    err.statusCode = status
    err.code = code
    cb(err)
    return cb.promise
}
