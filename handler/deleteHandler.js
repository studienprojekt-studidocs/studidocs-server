const path = './files'
const fs = require('fs')

exports.deleteFolder = deleteFolder

function deleteFolder(documentId) {
    if (fs.existsSync(path)) {
        if (fs.existsSync(path + '/' + documentId)) {
            fs.rmdirSync(path + '/' + documentId)
        }
    }
}
