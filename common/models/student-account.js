'use strict'

const utils = require('loopback/lib/utils')
const errorHandler = require('../../handler/errorHandler')

module.exports = function (Studentaccount) {
    Studentaccount.addStudentAccount = (data, req, res, cb) => {
        const AccessToken = Studentaccount.app.models.AccessToken
        const User = Studentaccount.app.models.userLogin
        cb = cb || utils.createPromiseCallback()

        const authPromise = new Promise((resolve, reject) => {
            if (req.headers.accesstoken) {
                // Validation of usertoken
                // eslint-disable-next-line max-len
                return AccessToken.findOne(
                    { where: { id: req.headers.accesstoken } },
                    (err, tokenEntry) => {
                        if (err) reject(err)
                        else if (!tokenEntry) {
                            // no token found -> normal user
                            // eslint-disable-next-line max-len
                            reject(
                                errorHandler.createError(
                                    'permissions needed to post',
                                    401,
                                    'REQUEST_DENIED',
                                ),
                            )
                        } else {
                            // Validation of token expiry date
                            var date = new Date(tokenEntry.created)
                            date.setSeconds(date.getSeconds() + tokenEntry.ttl)
                            if (date >= new Date()) {
                                // token is valid
                                // eslint-disable-next-line max-len
                                return User.findOne(
                                    { where: { id: tokenEntry.userId } },
                                    (err, userEntry) => {
                                        if (err) {
                                            cb(err)
                                            return cb.promise
                                        }
                                        if (!userEntry) {
                                            // no user was found
                                            // eslint-disable-next-line max-len
                                            reject(
                                                errorHandler.createError(
                                                    'permissions needed to post',
                                                    401,
                                                    'REQUEST_DENIED',
                                                ),
                                            )
                                        } else {
                                            // only accept userRole docent
                                            if (
                                                userEntry.userRole == 'docent'
                                            ) {
                                                resolve(userEntry)
                                            } else {
                                                reject(
                                                    errorHandler.createError(
                                                        'permissions needed to post',
                                                        401,
                                                        'REQUEST_DENIED',
                                                    ),
                                                )
                                            }
                                        }
                                    },
                                )
                            } else {
                                reject(
                                    errorHandler.createError(
                                        'permissions needed to post',
                                        401,
                                        'REQUEST_DENIED',
                                    ),
                                )
                            }
                        }
                    },
                )
            } else {
                reject(
                    errorHandler.createError(
                        'permissions needed to post',
                        401,
                        'REQUEST_DENIED',
                    ),
                )
            }
        })
        const parameterPromise = new Promise((resolve, reject) => {
            // Validation of parameter
            if (!data.studentName || typeof data.studentName != 'string')
                reject(
                    errorHandler.createError(
                        'parameter studentName required',
                        400,
                        'STUDENTNAME_REQUIRED',
                    ),
                )
            if (!data.expiryDate || typeof data.expiryDate != 'string')
                reject(
                    errorHandler.createError(
                        'parameter expiryDate required',
                        400,
                        'EXPIRYDATE_REQUIRED',
                    ),
                )
            if (new Date(data.expiryDate) <= new Date())
                reject(
                    errorHandler.createError(
                        'parameter expiryDate is invalid ',
                        400,
                        'EXPIRYDATE_INVALID',
                    ),
                )
            else {
                // check for valid studentName
                User.find(
                    { where: { userName: data.studentName } },
                    (err, foundUsers) => {
                        if (err) {
                            cb(err)
                            return cb.promise
                        } else if (foundUsers.length == 0) {
                            reject(
                                errorHandler.createError(
                                    'no user was found',
                                    400,
                                    'INVALID_STUDENTNAME',
                                ),
                            )
                        } else {
                            // validate the role of the user
                            if (foundUsers[0].userRole == 'student') {
                                const studentAccountEntry = {
                                    studentId: foundUsers[0].id,
                                    expiryDate: new Date(data.expiryDate),
                                    disabled: false,
                                }

                                resolve(studentAccountEntry)
                            } else {
                                reject(
                                    errorHandler.createError(
                                        'user is no student',
                                        400,
                                        'INVALID_STUDENTNAME',
                                    ),
                                )
                            }
                        }
                    },
                )
            }
        })

        return new Promise((resolve, reject) => {
            Promise.all([authPromise, parameterPromise])
                .then(([userEntry, studentAccountEntry]) => {
                    Studentaccount.findOne(
                        {
                            where: {
                                studentId: studentAccountEntry.studentId,
                                disabled: false,
                            },
                        },
                        (err, account) => {
                            if (err) {
                                cb(err)
                                return cb.promise
                            } else if (account) {
                                reject(
                                    errorHandler.createError(
                                        'the student exists already',
                                        400,
                                        'EXISTING_STUDENT',
                                    ),
                                )
                            } else {
                                Studentaccount.create(
                                    Object.assign(studentAccountEntry, {
                                        docentId: userEntry.id,
                                    }),
                                    (err, createdStudentAccount) => {
                                        if (err) {
                                            cb(err)
                                            return cb.promise
                                        } else {
                                            cb(null, createdStudentAccount)
                                            return cb.promise
                                        }
                                    },
                                )
                            }
                        },
                    )
                })
                .catch((error) => {
                    cb(error)
                    return cb.promise
                })
        })
    }

    Studentaccount.deleteStudentAccount = (data, req, res, cb) => {
        const AccessToken = Studentaccount.app.models.AccessToken
        const User = Studentaccount.app.models.userLogin
        const Document = Studentaccount.app.models.document
        const ReleaseRequest = Studentaccount.app.models.ReleaseRequest
        cb = cb || utils.createPromiseCallback()

        const authPromise = new Promise((resolve, reject) => {
            if (req.headers.accesstoken) {
                // Validation of usertoken
                return AccessToken.findOne(
                    { where: { id: req.headers.accesstoken } },
                    (err, tokenEntry) => {
                        if (err) reject(err)
                        else if (!tokenEntry) {
                            // no token found -> normal user
                            reject(
                                errorHandler.createError(
                                    'permissions needed to post',
                                    401,
                                    'REQUEST_DENIED',
                                ),
                            )
                        } else {
                            // Validation of token expiry date
                            var date = new Date(tokenEntry.created)
                            date.setSeconds(date.getSeconds() + tokenEntry.ttl)
                            if (date >= new Date()) {
                                // token is valid
                                return User.findOne(
                                    { where: { id: tokenEntry.userId } },
                                    (err, userEntry) => {
                                        if (err) reject(err)
                                        else if (!userEntry) {
                                            // no user was found
                                            reject(
                                                errorHandler.createError(
                                                    'permissions needed to post',
                                                    401,
                                                    'REQUEST_DENIED',
                                                ),
                                            )
                                        } else {
                                            // only accept userRole docent
                                            if (
                                                userEntry.userRole == 'docent'
                                            ) {
                                                resolve(userEntry)
                                            } else {
                                                reject(
                                                    errorHandler.createError(
                                                        'permissions needed to post',
                                                        401,
                                                        'REQUEST_DENIED',
                                                    ),
                                                )
                                            }
                                        }
                                    },
                                )
                            } else {
                                reject(
                                    errorHandler.createError(
                                        'permissions needed to post',
                                        401,
                                        'REQUEST_DENIED',
                                    ),
                                )
                            }
                        }
                    },
                )
            } else {
                reject(
                    errorHandler.createError(
                        'permissions needed to post',
                        401,
                        'REQUEST_DENIED',
                    ),
                )
            }
        })
        const parameterPromise = new Promise((resolve, reject) => {
            // get date in 14 days
            if (
                !data.studentAccountId ||
                typeof data.studentAccountId != 'string'
            )
                reject(
                    createError(
                        'parameter studentAccountId required',
                        400,
                        'STUDENTACCOUNTID_REQUIRED',
                    ),
                )
            else {
                // Validation of studentAccountId
                Studentaccount.findById(
                    data.studentAccountId,
                    (err, foundStudentAccount) => {
                        if (err) {
                            cb(err)
                            return cb.promise
                        } else if (!foundStudentAccount) {
                            reject(
                                'no studentAccount was found',
                                400,
                                'INVALID_STUDENTACCOUNTID',
                            )
                        } else {
                            resolve(foundStudentAccount)
                        }
                    },
                )
            }
        })

        Promise.all([authPromise, parameterPromise])
            .then(([userEntry, foundStudentAccount]) => {
                foundStudentAccount.updateAttributes(
                    { disabled: true },
                    (err, result) => {
                        if (err) {
                            cb(err)
                            return cb.promise
                        } else {
                            Document.find(
                                {
                                    where: {
                                        userId: foundStudentAccount.studentId,
                                    },
                                },
                                (err, result) => {
                                    if (result) {
                                        // maybe add that the release request only gets deleted when it's releaseState is 0
                                        /* ReleaseRequest.find({ where: { documentId: result.id, releaseState: 0 } }, (err, info) => {
                                    if (err) {
                                    } else {
                                    }
                                }) */
                                        ReleaseRequest.destroyAll(
                                            {
                                                documentId: result.id,
                                                releaseState: 0,
                                            },
                                            (err, info) => {
                                                if (err) {
                                                } else {
                                                }
                                            },
                                        )
                                    }
                                },
                            )
                            // maybe add the releaseRequest for destroy document
                            // eslint-disable-next-line max-len
                            Document.destroyAll(
                                {
                                    userId: foundStudentAccount.studentId,
                                    releaseState: 0,
                                },
                                (err, info) => {
                                    if (err) {
                                    } else {
                                        // delete document from container#
                                        // Franzi Dokument wird wegen hier nicht vom Server gelöscht, eintrag wird nur in der Datenbank gelöscht
                                        removeFile()
                                    }
                                },
                            )
                            cb(null, result)
                            return cb.promise
                        }
                    },
                )
            })
            .catch((error) => {
                cb(error)
                return cb.promise
            })
    }

    Studentaccount.refreshStudentAccount = (data, req, res, cb) => {
        const AccessToken = Studentaccount.app.models.AccessToken
        const User = Studentaccount.app.models.userLogin
        cb = cb || utils.createPromiseCallback()

        const authPromise = new Promise((resolve, reject) => {
            if (req.headers.accesstoken) {
                // Validation of usertoken
                return AccessToken.findOne(
                    { where: { id: req.headers.accesstoken } },
                    (err, tokenEntry) => {
                        if (err) reject(err)
                        else if (!tokenEntry) {
                            // no token found -> normal user
                            reject(
                                errorHandler.createError(
                                    'permissions needed to post',
                                    401,
                                    'REQUEST_DENIED',
                                ),
                            )
                        } else {
                            // Validation of token expiry date
                            var date = new Date(tokenEntry.created)
                            date.setSeconds(date.getSeconds() + tokenEntry.ttl)
                            if (date >= new Date()) {
                                // token is valid
                                return User.findOne(
                                    { where: { id: tokenEntry.userId } },
                                    (err, userEntry) => {
                                        if (err) reject(err)
                                        else if (!userEntry) {
                                            // no user was found
                                            reject(
                                                errorHandler.createError(
                                                    'permissions needed to post',
                                                    401,
                                                    'REQUEST_DENIED',
                                                ),
                                            )
                                        } else {
                                            // only accept userRole docent
                                            if (
                                                userEntry.userRole == 'docent'
                                            ) {
                                                resolve(userEntry)
                                            } else {
                                                reject(
                                                    errorHandler.createError(
                                                        'permissions needed to post',
                                                        401,
                                                        'REQUEST_DENIED',
                                                    ),
                                                )
                                            }
                                        }
                                    },
                                )
                            } else {
                                reject(
                                    errorHandler.createError(
                                        'permissions needed to post',
                                        401,
                                        'REQUEST_DENIED',
                                    ),
                                )
                            }
                        }
                    },
                )
            } else {
                reject(
                    errorHandler.createError(
                        'permissions needed to post',
                        401,
                        'REQUEST_DENIED',
                    ),
                )
            }
        })
        const parameterPromise = new Promise((resolve, reject) => {
            // get date in 14 days
            if (
                !data.studentAccountId ||
                typeof data.studentAccountId != 'string'
            )
                reject(
                    errorHandler.createError(
                        'parameter studentAccountId required',
                        400,
                        'STUDENTACCOUNTID_REQUIRED',
                    ),
                )
            if (!data.expiryDate || typeof data.expiryDate != 'string')
                reject(
                    errorHandler.createError(
                        'parameter expiryDate required',
                        400,
                        'EXPIRYDATE_REQUIRED',
                    ),
                )
            if (new Date(data.expiryDate) <= new Date())
                reject(
                    errorHandler.createError(
                        'parameter expiryDate is invalid ',
                        400,
                        'EXPIRYDATE_INVALID',
                    ),
                )
            else {
                // validate the existence of an account; is done below, but at the wrong place of coding
                // TODO
                const studentAccountEntry = {
                    expiryDate: new Date(data.expiryDate),
                }

                resolve(studentAccountEntry)
            }
        })

        Promise.all([authPromise, parameterPromise])
            .then(([userEntry, studentAccountEntry]) => {
                Studentaccount.findById(
                    data.studentAccountId,
                    (err, foundStudentAccount) => {
                        if (err) {
                            cb(err)
                            return cb.promise
                        } else if (!foundStudentAccount) {
                            cb(
                                errorHandler.createError(
                                    'no studentAccount was found',
                                    400,
                                    'INVALID_STUDENTACCOUNTID',
                                ),
                            )
                            return cb.promise
                        } else {
                            foundStudentAccount.updateAttributes(
                                studentAccountEntry,
                                (err, result) => {
                                    if (err) {
                                        cb(err)
                                        return cb.promise
                                    } else {
                                        cb(null, result)
                                        return cb.promise
                                    }
                                },
                            )
                        }
                    },
                )
            })
            .catch((error) => {
                cb(error)
                return cb.promise
            })
    }

    Studentaccount.studentAccountInfo = (req, res, cb) => {
        const AccessToken = Studentaccount.app.models.AccessToken
        const User = Studentaccount.app.models.userLogin
        cb = cb || utils.createPromiseCallback()

        const authPromise = new Promise((resolve, reject) => {
            if (req.headers.accesstoken) {
                // Validation of usertoken
                return AccessToken.findOne(
                    { where: { id: req.headers.accesstoken } },
                    (err, tokenEntry) => {
                        if (err) reject(err)
                        else if (!tokenEntry) {
                            // no token found -> normal user
                            reject(
                                errorHandler.createError(
                                    'permissions needed to get',
                                    401,
                                    'REQUEST_DENIED',
                                ),
                            )
                        } else {
                            // Validation of token expiry date
                            var date = new Date(tokenEntry.created)
                            date.setSeconds(date.getSeconds() + tokenEntry.ttl)
                            if (date >= new Date()) {
                                // token is valid
                                return User.findOne(
                                    { where: { id: tokenEntry.userId } },
                                    (err, userEntry) => {
                                        if (err) reject(err)
                                        else if (!userEntry) {
                                            // no user was found
                                            reject(
                                                errorHandler.createError(
                                                    'permissions needed to get',
                                                    401,
                                                    'REQUEST_DENIED',
                                                ),
                                            )
                                        } else {
                                            // only accept userRole docent
                                            if (
                                                userEntry.userRole == 'docent'
                                            ) {
                                                resolve(userEntry)
                                            } else {
                                                reject(
                                                    errorHandler.createError(
                                                        'permissions needed to get',
                                                        401,
                                                        'REQUEST_DENIED',
                                                    ),
                                                )
                                            }
                                        }
                                    },
                                )
                            } else {
                                reject(
                                    errorHandler.createError(
                                        'permissions needed to get',
                                        401,
                                        'REQUEST_DENIED',
                                    ),
                                )
                            }
                        }
                    },
                )
            } else {
                reject(
                    errorHandler.createError(
                        'permissions needed to get',
                        401,
                        'REQUEST_DENIED',
                    ),
                )
            }
        })

        authPromise
            .then((userEntry) => {
                // get all studentAccounts (also the expired) with their unfullified releaseRequests and belowing documents
                userEntry.managedStudentAccount(
                    {
                        where: { disabled: { neq: true } },
                        include: [
                            {
                                relation: 'authWithUserLogin',
                                scope: {
                                    fields: ['userName'],
                                },
                            },
                            {
                                relation: 'releaseRequests',
                                scope: {
                                    include: {
                                        relation: 'document',
                                        scope: {
                                            include: {
                                                relation: 'searchWords',
                                            },
                                        },
                                    },
                                },
                            },
                        ],
                    },
                    (err, studentAccountEntrys) => {
                        cb(null, studentAccountEntrys)
                        return cb.promise
                    },
                )
            })
            .catch((err) => {
                cb(err)
                return cb.promise
            })
    }

    Studentaccount.studentAccountById = (req, res, cb) => {
        const AccessToken = Studentaccount.app.models.AccessToken
        const User = Studentaccount.app.models.userLogin
        cb = cb || utils.createPromiseCallback()

        const authPromise = new Promise((resolve, reject) => {
            if (req.headers.accesstoken) {
                // Validation of usertoken
                return AccessToken.findOne(
                    { where: { id: req.headers.accesstoken } },
                    (err, tokenEntry) => {
                        if (err) reject(err)
                        else if (!tokenEntry) {
                            // no token found
                            reject(
                                errorHandler.createError(
                                    'permissions needed to upload',
                                    401,
                                    'UPLOAD_DENIED',
                                ),
                            )
                        } else {
                            // Validation of token expiry date
                            var date = new Date(tokenEntry.created)
                            date.setSeconds(date.getSeconds() + tokenEntry.ttl)
                            if (date >= new Date()) {
                                // token is valid
                                return User.findOne(
                                    { where: { id: tokenEntry.userId } },
                                    (err, userEntry) => {
                                        if (err) reject(err)
                                        else if (!userEntry) {
                                            reject(
                                                errorHandler.createError(
                                                    'user not found',
                                                    401,
                                                    'UPLOAD_DENIED',
                                                ),
                                            )
                                        } else {
                                            // validation of studentAccount if user is student
                                            if (
                                                userEntry.userRole == 'student'
                                            ) {
                                                resolve(userEntry)
                                            } else {
                                                // user is no student
                                                resolve(userEntry)
                                            }
                                        }
                                    },
                                )
                            } else {
                                reject(
                                    errorHandler.createError(
                                        'token is expired',
                                        401,
                                        'TOKEN_EXPIRED',
                                    ),
                                )
                            }
                        }
                    },
                )
            } else {
                reject(
                    errorHandler.createError(
                        'permissions needed to upload',
                        401,
                        'UPLOAD_DENIED',
                    ),
                )
            }
        })

        return new Promise((resolve, reject) => {
            Promise.all([authPromise]).then(([userEntry]) => {
                if (userEntry.userRole === 'docent') {
                    resolve({ id: '1' })
                }
                Studentaccount.findOne(
                    { where: { studentId: req.query.id, disabled: false } },
                    (err, studentAccount) => {
                        if (err) {
                            reject(err)
                            return cb.promise
                        } else if (studentAccount) {
                            resolve(studentAccount)
                        } else {
                            resolve({ id: '0' })
                        }
                    },
                )
            })
        })
    }

    Studentaccount.setup = function () {
        // We need to call the base class's setup method
        Studentaccount.base.setup.call(this)
        var StudentaccountModel = this

        StudentaccountModel.remoteMethod('addStudentAccount', {
            description: 'Activates the UserLogin of a student for 14 days',
            accepts: [
                {
                    arg: 'data',
                    type: 'object',
                    required: true,
                    http: { source: 'body' },
                },
                { arg: 'req', type: 'object', http: { source: 'req' } },
                { arg: 'res', type: 'object', http: { source: 'res' } },
            ],
            returns: { arg: 'fileObject', type: 'object', root: true },
            http: { verb: 'post' },
        })

        StudentaccountModel.remoteMethod('deleteStudentAccount', {
            description:
                'Deletes the given studentAccount with their releaseRequests and unreleased documents',
            accepts: [
                {
                    arg: 'data',
                    type: 'object',
                    required: true,
                    http: { source: 'body' },
                },
                { arg: 'req', type: 'object', http: { source: 'req' } },
                { arg: 'res', type: 'object', http: { source: 'res' } },
            ],
            returns: { arg: 'fileObject', type: 'object', root: true },
            http: { verb: 'post' },
        })

        StudentaccountModel.remoteMethod('refreshStudentAccount', {
            description:
                'Refreshes the expiryDate of the studentAccount so it is valid for 14 day from today',
            accepts: [
                {
                    arg: 'data',
                    type: 'object',
                    required: true,
                    http: { source: 'body' },
                },
                { arg: 'req', type: 'object', http: { source: 'req' } },
                { arg: 'res', type: 'object', http: { source: 'res' } },
            ],
            returns: { arg: 'fileObject', type: 'object', root: true },
            http: { verb: 'post' },
        })

        StudentaccountModel.remoteMethod('studentAccountInfo', {
            description:
                'Get all studentAccounts, which are not disabled and corresponding to the docent with their releaseRequests and the belowing documents',
            accepts: [
                { arg: 'req', type: 'object', http: { source: 'req' } },
                { arg: 'res', type: 'object', http: { source: 'res' } },
            ],
            returns: { arg: 'documents', type: 'object', root: true },
            http: { verb: 'get' },
        })

        Studentaccount.remoteMethod('studentAccountById', {
            description: 'Get a student account by id',
            accepts: [
                { arg: 'req', type: 'object', http: { source: 'req' } },
                { arg: 'res', type: 'object', http: { source: 'res' } },
            ],
            returns: { arg: 'studentAccount', type: 'object', root: true },
            http: { verb: 'get' },
        })

        return StudentaccountModel
    }

    Studentaccount.setup()
}

function removeFile() {}
