'use strict'
var axios = require('axios')
var qs = require('querystring')
const utils = require('loopback/lib/utils')
const errorHandler = require('../../handler/errorHandler')

module.exports = function (Userlogin) {
    /**
     * Create access token for the logged in user.
     * @options {Number|Object} [ttl|data] Either the requested ttl,
     * or an object with token properties to set (see below).
     * @property {Number} [ttl] The requested ttl
     * @property {String[]} [scopes] The access scopes granted to the token.
     * @param {Object} [options] Additional options including remoting context
     * @callback {Function} cb The callback function
     * @param {String|Error} err The error string or object
     * @param {AccessToken} token The generated access token object
     * @promise
     *
     */

    Userlogin.prototype.createAccessToken = function (ttl, cb) {
        cb = cb || utils.createPromiseCallback()

        let tokenData = { ttl }

        var userSettings = this.constructor.settings
        tokenData.ttl = Math.min(
            tokenData.ttl || userSettings.ttl,
            userSettings.maxTTL,
        )
        this.accessTokens.create(tokenData, cb)
        return cb.promise
    }

    /**
     * Login a user by with the given `credentials`.
     * @param {Object} credentials username/password or email/password
     * @param {String[]|String} [include] Optionally set it to "user" to include
     * the user info
     * @callback {Function} callback Callback function
     * @param {Error} err Error object
     * @param {AccessToken} token Access token if login is successful
     * @promise
     */

    Userlogin.login = function (credentials, include, fn) {
        var self = this
        if (typeof include === 'function') {
            fn = include
            include = undefined
        }

        fn = fn || utils.createPromiseCallback()

        if (!credentials.username) {
            return errorHandler.ErrorLogging(
                'username is required',
                400,
                'USERNAME_REQUIRED',
                fn,
            )
        }
        if (!credentials.username && typeof credentials.username !== 'string') {
            return errorHandler.ErrorLogging(
                'invalid username',
                400,
                'INVALID_USERNAME',
                fn,
            )
        }
        //Post Request against HWR webmail
        const requestConfig = {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            timeout: 100,
        }
        const requestBody = {
            name: credentials.username,
            password: credentials.password,
        }
        const requestUrl =
            'https://webmail.stud.hwr-berlin.de/ajax/login?action=login'

        /* development testaccount -> no auth with webmail */
        /* HAS TO BE DELETED IN PROD */ //TODO
        const devUser = {
            userName: 'Testaccount',
            userRole: 'docent',
        }
        if (credentials.username == devUser.userName) {
            self.findOne(
                { where: { userName: devUser.userName } },
                (err, user) => {
                    if (err) {
                        //error during search
                        return errorHandler.ErrorLogging(
                            'Server failure',
                            500,
                            'SERVER_FAILURE',
                            fn,
                        )
                    } else {
                        if (!user) {
                            //user doesn't exist in intern table
                            self.create(
                                {
                                    userName: devUser.userName,
                                    userRole: devUser.userRole,
                                },
                                (err, user) => {
                                    if (err)
                                        return errorHandler.ErrorLogging(
                                            'Server failure',
                                            500,
                                            'SERVER_FAILURE',
                                            fn,
                                        )
                                    else {
                                        if (!user)
                                            return errorHandler.ErrorLogging(
                                                'creation of user failed',
                                                500,
                                                'CREATION_FAILURE',
                                                fn,
                                            )
                                        else {
                                            user.createAccessToken(
                                                credentials.ttl,
                                                (err, token) => {
                                                    if (err) return fn(err)
                                                    fn(err, {
                                                        userId: token.userId,
                                                        userName: user.userName,
                                                        userRole: user.userRole,
                                                        token: token.id,
                                                    })
                                                },
                                            )
                                        }
                                    }
                                },
                            )
                        } else {
                            user.createAccessToken(
                                credentials.ttl,
                                (err, token) => {
                                    if (err) return fn(err)
                                    fn(err, {
                                        userId: token.userId,
                                        userName: user.userName,
                                        userRole: user.userRole,
                                        token: token.id,
                                    })
                                },
                            )
                        }
                    }
                },
            )
        } else {
            /* prod auth against hwr webmail server */
            axios
                .post(requestUrl, qs.stringify(requestBody), requestConfig)
                .then((response) => {
                    if (response.data.user) {
                        /* successful request
                         * reponseJSON: session: sessionString, user: userName, user_id: userId, context_id: someId (purpose unkown), locale: localString (de_DE)
                         */
                        //find user in userLogin table
                        self.findOne(
                            { where: { userName: response.data.user } },
                            (err, user) => {
                                if (err) {
                                    //error during search
                                    return errorHandler.ErrorLogging(
                                        'Server failure',
                                        500,
                                        'SERVER_FAILURE',
                                        fn,
                                    )
                                } else {
                                    if (!user) {
                                        //user doesn't exist in intern table
                                        var userRole = getUserRole(
                                            response.data.user,
                                        )
                                        if (!userRole) {
                                            return errorHandler.ErrorLogging(
                                                'user role could not be determined',
                                                500,
                                                'NO_MATCHING_USERROLE',
                                                fn,
                                            )
                                        } else {
                                            self.create(
                                                {
                                                    userName:
                                                        response.data.user,
                                                    userRole: userRole,
                                                },
                                                (err, user) => {
                                                    if (err)
                                                        return errorHandler.ErrorLogging(
                                                            'Server failure',
                                                            500,
                                                            'SERVER_FAILURE',
                                                            fn,
                                                        )
                                                    else {
                                                        if (!user)
                                                            return errorHandler.ErrorLogging(
                                                                'creation of user failed',
                                                                500,
                                                                'CREATION_FAILURE',
                                                                fn,
                                                            )
                                                        else {
                                                            user.createAccessToken(
                                                                credentials.ttl,
                                                                (
                                                                    err,
                                                                    token,
                                                                ) => {
                                                                    if (err)
                                                                        return fn(
                                                                            err,
                                                                        )
                                                                    fn(err, {
                                                                        userId: token.userId,
                                                                        userName:
                                                                            user.userName,
                                                                        userRole:
                                                                            user.userRole,
                                                                        token: token.id,
                                                                    })
                                                                },
                                                            )
                                                        }
                                                    }
                                                },
                                            )
                                        }
                                    } else {
                                        user.createAccessToken(
                                            credentials.ttl,
                                            (err, token) => {
                                                if (err) return fn(err)
                                                fn(err, {
                                                    userId: token.userId,
                                                    userName: user.userName,
                                                    userRole: user.userRole,
                                                    token: token.id,
                                                })
                                            },
                                        )
                                    }
                                }
                            },
                        )
                    } else {
                        if (response.data.code == 'LGI-0006') {
                            return errorHandler.ErrorLogging(
                                'wrong username or password',
                                400,
                                'INVALID_CREDENTIALS',
                                fn,
                            )
                        } else {
                            return errorHandler.ErrorLogging(
                                'unexpected login error',
                                400,
                                'LOGIN_FAILURE',
                                fn,
                            )
                        }
                    }
                })
                .catch((err) => {
                    return errorHandler.ErrorLogging(
                        'cant get response from auth-server',
                        500,
                        'SERVER_FAILURE',
                    )
                })
        }

        return fn.promise
    }

    /**
     * Logout a user with the given accessToken id.
     * @param {String} accessTokenID
     * @callback {Function} callback
     * @param {Error} err
     * @promise
     */

    Userlogin.logout = function (tokenId, fn) {
        fn = fn || utils.createPromiseCallback()

        var err
        if (!tokenId) {
            err = new Error('accessToken is required to logout')
            err.statusCode = 401
            process.nextTick(fn, err)
            return fn.promise
        }

        this.relations.accessTokens.modelTo.destroyById(
            tokenId,
            function (err, info) {
                if (err) {
                    fn(err)
                } else if ('count' in info && info.count === 0) {
                    err = new Error('Could not find accessToken')
                    err.statusCode = 401
                    fn(err)
                } else {
                    fn()
                }
            },
        )
        return fn.promise
    }

    Userlogin.setup = function () {
        // We need to call the base class's setup method
        Userlogin.base.setup.call(this)
        var UserloginModel = this

        UserloginModel.remoteMethod('login', {
            description:
                'Send login request to HWR webmail and login a user with username and password.',
            accepts: [
                {
                    arg: 'credentials',
                    type: 'object',
                    required: true,
                    http: { source: 'body' },
                },
                {
                    arg: 'include',
                    type: ['string'],
                    http: { source: 'query' },
                    description:
                        'Related objects to include in the response. ' +
                        'See the description of return value for more details.',
                },
            ],
            returns: {
                arg: 'accessToken',
                type: 'object',
                root: true,
                description:
                    'The response body contains properties of the AccessToken created on login.',
            },
            http: { verb: 'post' },
        })

        UserloginModel.remoteMethod('logout', {
            description: 'Logout a user with access token.',
            accepts: [
                {
                    arg: 'access_token',
                    type: 'string',
                    http: function (ctx) {
                        var req = ctx && ctx.req
                        var accessToken = req && req.accessToken
                        var tokenID = accessToken ? accessToken.id : undefined

                        return tokenID
                    },
                    description:
                        'Do not supply this argument, it is automatically extracted ' +
                        'from request headers.',
                },
            ],
            http: { verb: 'all' },
        })

        return UserloginModel
    }

    Userlogin.setup()
}

//get corresponding user role from username
function getUserRole(username) {
    if (username[0] === 's'){
        if (username[1] === '_') {
            return 'student'
        }
        else {
            return 'docent'
        }
    }
    else{
        return 'docent'
    }
}
