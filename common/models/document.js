'use strict'

const fs = require('fs')
const fs_extra = require('fs-extra')
const formidable = require('formidable')
const utils = require('loopback/lib/utils')
const errorHandler = require('../../handler/errorHandler')
const deleteHandler = require('../../handler/deleteHandler')

const BUCKET = 'temp'

module.exports = function (Document) {
    Document.download = (data, req, res, cb) => {
        const AccessToken = Document.app.models.AccessToken
        const User = Document.app.models.userLogin
        cb = cb || utils.createPromiseCallback()
        let documentName = ''
        //get document entry to determine the required role to download and see
        const authPromise = new Promise((resolve, reject) => {
            Document.findOne(
                { where: { id: data.container } },
                (err, document) => {
                    if (err) reject(err)
                    else {
                        documentName = document.title
                        if (req.headers.accesstoken) {
                            //Validation of usertoken
                            return AccessToken.findOne(
                                { where: { id: req.headers.accesstoken } },
                                (err, tokenEntry) => {
                                    if (err) reject(err)
                                    else if (!tokenEntry) {
                                        //no token found and privacy state above "everyone can see"
                                        if (document.privacyState != 0) {
                                            reject(
                                                errorHandler.createError(
                                                    'permissions needed to get the file',
                                                    401,
                                                    'DOWNLOAD_DENIED',
                                                ),
                                            )
                                        } else {
                                            resolve()
                                        }
                                    } else {
                                        //Validation of token expiry date
                                        var date = new Date(tokenEntry.created)
                                        date.setSeconds(
                                            date.getSeconds() + tokenEntry.ttl,
                                        )
                                        if (date >= new Date()) {
                                            //token is valid
                                            return User.findOne(
                                                {
                                                    where: {
                                                        id: tokenEntry.userId,
                                                    },
                                                },
                                                (err, userEntry) => {
                                                    if (err) reject(err)
                                                    else if (!userEntry) {
                                                        reject(
                                                            errorHandler.createError(
                                                                'user not found',
                                                                401,
                                                                'DOWNLOAD_DENIED',
                                                            ),
                                                        )
                                                    } else {
                                                        //compare user role with required role
                                                        if (
                                                            document.userId ==
                                                                userEntry.id &&
                                                            getRoleNumber(
                                                                userEntry.userRole,
                                                            ) <
                                                                document.privacyState
                                                        ) {
                                                            reject(
                                                                errorHandler.createError(
                                                                    'permissions needed to get the file',
                                                                    401,
                                                                    'DOWNLOAD_DENIED',
                                                                ),
                                                            )
                                                        } else {
                                                            resolve()
                                                        }
                                                    }
                                                },
                                            )
                                        } else {
                                            reject(
                                                errorHandler.createError(
                                                    'token is expired',
                                                    401,
                                                    'TOKEN_EXPIRED',
                                                ),
                                            )
                                        }
                                    }
                                },
                            )
                        } else {
                            if (document.privacyState != 0) {
                                reject(
                                    errorHandler.createError(
                                        'permissions needed to get the file',
                                        401,
                                        'DOWNLOAD_DENIED',
                                    ),
                                )
                            } else {
                                resolve()
                            }
                        }
                    }
                },
            )
        })
        //create promise to check if the document is released
        //TODO

        Promise.all([authPromise])
            .then(([]) => {
                //Validation of parameters
                if (!data.container || typeof data.container != 'string')
                    cb(
                        errorHandler.createError(
                            'parameter container required',
                            400,
                            'CONTAINER_REQUIRED',
                        ),
                    )
                else if (!data.file || typeof data.file != 'string')
                    cb(
                        errorHandler.createError(
                            'parameter file required',
                            400,
                            'FILE_REQUIRED',
                        ),
                    )
                else {
                    //get file from container
                    //Container uses datasource storage see: https://loopback.io/doc/en/lb3/Storage-example.html for more informations
                    res.header(
                        'Access-Control-Expose-Headers',
                        'Content-Disposition',
                    )
                    res.header(
                        'Content-Disposition',
                        "attachment; title='" + documentName + ".pdf'",
                    )
                    const Container = Document.app.models.documentContainer
                    Container.download(data.container, data.file, req, res, cb)

                    //increment download counter in documents entry
                    //TODO
                }
                return cb.promise
            })
            .catch((err) => {
                cb(err)
                return cb.promise
            })
    }

    Document.upload = (req, res, body, cb) => {
        //creates bucket-folder if not exist
        //bucket folder is for temp saving
        const { name: storageName, root: storageRoot } =
            Document.app.dataSources.storage.settings
        checkBucketFolder(storageName, storageRoot)
        checkForCorrectContentType(req)

        cb = cb || utils.createPromiseCallback()

        const form = new formidable()
        const AccessToken = Document.app.models.AccessToken
        const headerAccessToken = req.headers.accesstoken
        const User = Document.app.models.userLogin
        const authPromise = createAuthPromise(
            AccessToken,
            User,
            headerAccessToken,
        )
        const fieldsPromise = createFieldsPromise(req, form)
        const documentEntryPromise = createDocumentEntryPromise(
            authPromise,
            fieldsPromise,
        )
        //Container uses datasource storage see: https://loopback.io/doc/en/lb3/Storage-example.html for more informations
        const Container = Document.app.models.documentContainer
        const filePromise = createFilePromise(req, Container, res)

        Promise.all([documentEntryPromise, filePromise])
            .then(([[documentEntry, searchwords], fileObj]) => {
                //creation of document-table entry
                Document.create(documentEntry, (error, document) => {
                    if (error) {
                        Container.removeFile(BUCKET, documentEntry.fileName)
                        cb(error)
                        return cb.promise
                    } else {
                        //get user information from creator
                        document.userLogin((err, user) => {
                            if (err) {
                                Container.removeFile(
                                    BUCKET,
                                    documentEntry.fileName,
                                )
                                cb(err)
                                return cb.promise
                            } else {
                                if (user.userRole == 'student') {
                                    //get all studentAccount from user where expiry date is greater then today
                                    user.authWithStudentAccount(
                                        {
                                            where: {
                                                expiryDate: { gt: new Date() },
                                            },
                                        },
                                        (err, studentAccounts) => {
                                            if (err) {
                                                Container.removeFile(
                                                    BUCKET,
                                                    documentEntry.fileName,
                                                )
                                                cb(err)
                                                return cb.promise
                                            } else {
                                                if (
                                                    studentAccounts.length == 0
                                                ) {
                                                    //no valid studentAccount was found => delete document entry and file
                                                    deleteDocumentEntryAndFile(
                                                        Document,
                                                        document,
                                                        Container,
                                                        documentEntry,
                                                        cb,
                                                    )
                                                } else {
                                                    //take first element of studentAccounts-array to create releaseRequest for this account
                                                    document.releaseRequests.create(
                                                        {
                                                            studentAccountId:
                                                                studentAccounts[0]
                                                                    .id,
                                                            releaseState: 0,
                                                        },
                                                        (
                                                            err,
                                                            releaseRequest,
                                                        ) => {
                                                            if (err) {
                                                                Container.removeFile(
                                                                    BUCKET,
                                                                    documentEntry.fileName,
                                                                )
                                                                cb(err)
                                                                return cb.promise
                                                            } else {
                                                                //move file to new container (document.id) => problem of duplication in names solved
                                                                let outputDir = `${storageRoot}//${document.id}`
                                                                if (
                                                                    !fs.existsSync(
                                                                        outputDir,
                                                                    )
                                                                ) {
                                                                    fs.mkdirSync(
                                                                        outputDir,
                                                                    )
                                                                }
                                                                //Franzi BUCKET löschen?
                                                                fs_extra.move(
                                                                    `${storageRoot}${BUCKET}//${documentEntry.fileName}`,
                                                                    `${outputDir}//${documentEntry.fileName}`,
                                                                    (
                                                                        err,
                                                                        result,
                                                                    ) => {},
                                                                )
                                                                //searchwords = ['fdsfdsf','fdsfdf',...]
                                                                const SearchWords =
                                                                    Document.app
                                                                        .models
                                                                        .searchWord
                                                                addSearchwords(
                                                                    document,
                                                                    searchwords,
                                                                    cb,
                                                                    SearchWords,
                                                                )
                                                            }
                                                        },
                                                    )
                                                }
                                            }
                                        },
                                    )
                                } else {
                                    let outputDir = `${storageRoot}//${document.id}`
                                    if (!fs.existsSync(outputDir)) {
                                        fs.mkdirSync(outputDir)
                                    }
                                    //Franzi BUCKET löschen?
                                    fs_extra.move(
                                        `${storageRoot}${BUCKET}//${documentEntry.fileName}`,
                                        `${outputDir}//${documentEntry.fileName}`,
                                        (err, result) => {},
                                    )
                                    const SearchWords =
                                        Document.app.models.searchWord
                                    addSearchwords(
                                        document,
                                        searchwords,
                                        cb,
                                        SearchWords,
                                    )
                                }
                                cb(null, document)
                                return cb.promise
                            }
                        })
                    }
                })
            })
            .catch((error) => {
                //if one promise isn't resolving delete uploaded file
                form.parse(req, (error, fields, files) => {
                    Container.removeFile(BUCKET, files.file.name)
                })
                cb(error)
                return cb.promise
            })
    }

    Document.documentInfo = (req, res, cb) => {
        const AccessToken = Document.app.models.AccessToken
        const User = Document.app.models.userLogin
        cb = cb || utils.createPromiseCallback()

        const skipFilter = req.query.filter.skip

        const authPromise = new Promise((resolve, reject) => {
            if (req.headers.accesstoken) {
                //Validation of usertoken
                return AccessToken.findOne(
                    { where: { id: req.headers.accesstoken } },
                    (err, tokenEntry) => {
                        if (err) reject(err)
                        else if (!tokenEntry) {
                            //no token found -> normal user
                            resolve(0)
                        } else {
                            //Validation of token expiry date
                            var date = new Date(tokenEntry.created)
                            date.setSeconds(date.getSeconds() + tokenEntry.ttl)
                            if (date >= new Date()) {
                                //token is valid
                                return User.findOne(
                                    { where: { id: tokenEntry.userId } },
                                    (err, userEntry) => {
                                        if (err) reject(err)
                                        else if (!userEntry) {
                                            //no user was found
                                            resolve(0)
                                        } else {
                                            resolve(
                                                getRoleNumber(
                                                    userEntry.userRole,
                                                ),
                                            )
                                        }
                                    },
                                )
                            } else {
                                resolve(0)
                            }
                        }
                    },
                )
            } else {
                resolve(0)
            }
        })

        authPromise
            .then((userRole) => {
                let documentCount
                Document.count(
                    { releaseState: 1, privacyState: { lte: userRole } },
                    (err, count) => {
                        documentCount = count
                    },
                )
                Document.find(
                    {
                        include: [
                            {
                                relation: 'releaseRequests',
                                scope: { where: { releaseState: 1 } },
                            },
                            {
                                relation: 'searchWords',
                            },
                        ],
                        where: { privacyState: { lte: userRole } },
                        limit: 40,
                        skip: skipFilter,
                    },
                    (err, results) => {
                        if (err) {
                            cb(err)
                            return cb.promise
                        }
                        //parse the results -> otherwise result.releaseReqeuests would be the reference to the model object (because of the relation)
                        results = JSON.parse(JSON.stringify(results))
                        results = results.filter((result) => {
                            return result.releaseRequests != undefined
                        })
                        cb(null, { results, documentCount })
                        return cb.promise
                    },
                )
            })
            .catch((error) => {
                cb(err)
                return cb.promise
            })
    }

    Document.ownDocumentInfo = (req, res, cb) => {
        const AccessToken = Document.app.models.AccessToken
        const User = Document.app.models.userLogin
        cb = cb || utils.createPromiseCallback()

        const authPromise = new Promise((resolve, reject) => {
            if (req.headers.accesstoken) {
                //Validation of usertoken
                return AccessToken.findOne(
                    { where: { id: req.headers.accesstoken } },
                    (err, tokenEntry) => {
                        if (err) reject(err)
                        else if (!tokenEntry) {
                            //no token found -> normal user
                            reject(
                                errorHandler.createError(
                                    'permissions needed to get the files',
                                    401,
                                    'OWNDOCUMENTS_DENIED',
                                ),
                            )
                        } else {
                            return User.findOne(
                                { where: { id: tokenEntry.userId } },
                                (err, userEntry) => {
                                    if (err) reject(err)
                                    else if (!userEntry) {
                                        //no user was found
                                        reject(
                                            errorHandler.createError(
                                                'user not found',
                                                401,
                                                'OWNDOCUMENTS_DENIED',
                                            ),
                                        )
                                    } else {
                                        resolve(userEntry)
                                    }
                                },
                            )
                        }
                    },
                )
            } else {
                resolve({})
                //check why this has to be here
                /* reject(errorHandler.createError('permissions needed to get the files', 401, 'OWNDOCUMENTS_DENIED')) */
            }
        })

        authPromise
            .then((userEntry) => {
                if (userEntry.id) {
                    userEntry.documents(
                        {
                            include: [
                                {
                                    relation: 'releaseRequests',
                                },
                                {
                                    relation: 'searchWords',
                                },
                            ],
                            where: { userId: userEntry.id },
                        },
                        (err, results) => {
                            //parse the results -> otherwise result.releaseReqeuests would be the reference to the model object (because of the relation)
                            results = JSON.parse(JSON.stringify(results))
                            results = results.filter((result) => {
                                return result.releaseRequests != undefined
                            })
                            cb(null, results)
                            return cb.promise
                        },
                    )
                }
            })
            .catch((err) => {
                cb(err)
                return cb.promise
            })
    }

    Document.deleteDocument = (req, res, body, cb) => {
        const { name: storageName, root: storageRoot } =
            Document.app.dataSources.storage.settings
        checkBucketFolder(storageName, storageRoot)
        checkForCorrectContentType(req)

        cb = cb || utils.createPromiseCallback()

        const Container = Document.app.models.documentContainer
        const ReleaseRequest = Document.app.models.releaseRequest
        const form = new formidable()
        const AccessToken = Document.app.models.AccessToken
        const headerAccessToken = req.headers.accesstoken
        const User = Document.app.models.userLogin
        const authPromise = createAuthPromise(
            AccessToken,
            User,
            headerAccessToken,
        )
        const fieldsPromise = createDeletePromise(req, form)
        const deleteDocumentEntryPromise =
            createDeleteDocumentEntryPromise(fieldsPromise)

        Promise.all([deleteDocumentEntryPromise]).then(([[documentEntry]]) => {
            //Franzi
            Document.findById(documentEntry.id, (err, result) => {
                if (err) {
                    cb(err)
                    return cb.promise
                } else {
                    //Franzi
                    let outputDir = `${storageRoot}//${documentEntry.id}`
                    removeFile(
                        Container,
                        documentEntry.id,
                        result.fileName,
                        outputDir,
                    )
                    cb(null, document)
                    return cb.promise
                }
            })
            deleteDocumentEntry(Document, documentEntry)
            deleteReleaseReqeuest(ReleaseRequest, documentEntry.id)
        })
    }

    Document.updateDocument = (req, res, body, cb) => {
        //change documentInfo/ delete old document in folder for the new one
        //in all cases set the releaseState of the releaseRequest back to 0
        //TODO
        const { name: storageName, root: storageRoot } =
            Document.app.dataSources.storage.settings
        checkBucketFolder(storageName, storageRoot)
        checkForCorrectContentType(req)

        cb = cb || utils.createPromiseCallback()

        const form = new formidable()
        const AccessToken = Document.app.models.AccessToken
        const Container = Document.app.models.documentContainer
        const headerAccessToken = req.headers.accesstoken
        const User = Document.app.models.userLogin
        const authPromise = createAuthPromise(
            AccessToken,
            User,
            headerAccessToken,
        )
        const fieldsPromise = createUpdateFieldsPromise(req, form)
        const documentEntryPromise = createUpdateDocumentEntryPromise(
            authPromise,
            fieldsPromise,
        )

        const filePromise = createUpdateFilePromise(req, Container, res, cb)

        Promise.all([documentEntryPromise, filePromise]).then(
            ([[documentEntry, searchwords, getFileUpdated], fileObj]) => {
                Document.findById(documentEntry.id, (err, result) => {
                    const oldFilename = result.fileName
                    if (err) {
                        cb(err)
                        return cb.promise
                    }
                    result.updateAttributes(
                        documentEntry,
                        (err, updatedDocument) => {
                            if (err) {
                                cb(err)
                                return cb.promise
                            }
                            cb(null, updatedDocument)
                            if (getFileUpdated === true) {
                                removeUpdateFile(
                                    Container,
                                    documentEntry.id,
                                    oldFilename,
                                )
                                let outputDir = `${storageRoot}//${documentEntry.id}`
                                if (!fs.existsSync(outputDir)) {
                                    fs.mkdirSync(outputDir)
                                }
                                //Franzi BUCKET löschen?
                                fs_extra.move(
                                    `${storageRoot}${BUCKET}//${documentEntry.fileName}`,
                                    `${outputDir}//${documentEntry.fileName}`,
                                    (err, result) => {},
                                )
                            }
                            const SearchWords = Document.app.models.searchWord
                            addSearchwords(result, searchwords, cb, SearchWords)
                            return cb.promise
                        },
                    )
                })
            },
        )
    }

    Document.filterDocuments = (req, res, cb) => {
        cb = cb || utils.createPromiseCallback()

        const AccessToken = Document.app.models.AccessToken
        const headerAccessToken = req.headers.accesstoken
        const User = Document.app.models.userLogin
        const authPromise = createFilterAuthPromise(
            AccessToken,
            User,
            headerAccessToken,
        )

        authPromise
            .then((userRole) => {
                //get all documents the user is allowed to see
                let filter = JSON.parse(req.query.filter[0])
                filter.where.and.push({ privacyState: { lte: userRole } })
                const concatFilter = {
                    include: [
                        {
                            relation: 'releaseRequests',
                            scope: { where: { releaseState: 1 } },
                        },
                        {
                            relation: 'searchWords',
                        },
                    ],
                    ...filter,
                    limit: 40,
                    skip: req.query.filter[1].skip,
                }
                Document.find(concatFilter, (err, results) => {
                    results = JSON.parse(JSON.stringify(results))
                    results = results.filter((result) => {
                        return result.releaseRequests != undefined
                    })
                    cb(null, results)
                    return cb.promise
                })
            })
            .catch((err) => {
                cb(err)
                return cb.promise
            })
    }

    Document.getDocumentCount = (req, res, cb) => {
        const AccessToken = Document.app.models.AccessToken
        const User = Document.app.models.userLogin
        const ReleaseRequest = Document.app.models.releaseRequest
        cb = cb || utils.createPromiseCallback()

        const authPromise = new Promise((resolve, reject) => {
            if (req.headers.accesstoken) {
                //Validation of usertoken
                return AccessToken.findOne(
                    { where: { id: req.headers.accesstoken } },
                    (err, tokenEntry) => {
                        if (err) reject(err)
                        else if (!tokenEntry) {
                            //no token found -> normal user
                            resolve(0)
                        } else {
                            //Validation of token expiry date
                            var date = new Date(tokenEntry.created)
                            date.setSeconds(date.getSeconds() + tokenEntry.ttl)
                            if (date >= new Date()) {
                                //token is valid
                                return User.findOne(
                                    { where: { id: tokenEntry.userId } },
                                    (err, userEntry) => {
                                        if (err) reject(err)
                                        else if (!userEntry) {
                                            //no user was found
                                            resolve(0)
                                        } else {
                                            resolve(
                                                getRoleNumber(
                                                    userEntry.userRole,
                                                ),
                                            )
                                        }
                                    },
                                )
                            } else {
                                resolve(0)
                            }
                        }
                    },
                )
            } else {
                resolve(0)
            }
        })

        authPromise
            .then((userRole) => {
                const concatFilter = {
                    include: [
                        {
                            relation: 'releaseRequests',
                            scope: { where: { releaseState: 1 } },
                        },
                        {
                            relation: 'searchWords',
                        },
                    ],
                    privacyState: { lte: userRole },
                }
                Document.find(concatFilter, (err, results) => {
                    results = JSON.parse(JSON.stringify(results))
                    results = results.filter((result) => {
                        return result.releaseRequests != undefined
                    })
                    cb(null, results.length)
                })
            })
            .catch((err) => {
                cb(err)
                return cb.promise
            })
    }

    Document.setup = function () {
        // We need to call the base class's setup method
        Document.base.setup.call(this)
        var DocumentModel = this

        DocumentModel.remoteMethod('upload', {
            description: 'Uploads a file',
            accepts: [
                { arg: 'req', type: 'object', http: { source: 'req' } },
                { arg: 'res', type: 'object', http: { source: 'res' } },
                { arg: 'body', type: 'object', http: { source: 'body' } },
            ],
            returns: { arg: 'fileObject', type: 'object', root: true },
            http: { verb: 'post' },
        })

        DocumentModel.remoteMethod('download', {
            description: 'Downloads a file',
            accepts: [
                {
                    arg: 'data',
                    type: 'object',
                    required: true,
                    http: { source: 'body' },
                },
                { arg: 'req', type: 'object', http: { source: 'req' } },
                { arg: 'res', type: 'object', http: { source: 'res' } },
            ],
            http: { verb: 'post' },
        })

        DocumentModel.remoteMethod('documentInfo', {
            description:
                'Get all document entries that the user is allowed to see and which dont have a active releaseRequest',
            accepts: [
                { arg: 'req', type: 'object', http: { source: 'req' } },
                { arg: 'res', type: 'object', http: { source: 'res' } },
            ],
            returns: { arg: 'documents', type: 'object', root: true },
            http: { verb: 'get' },
        })

        DocumentModel.remoteMethod('ownDocumentInfo', {
            description:
                'Get all document entries that the user has uploaded with the corresponding releaseRequest',
            accepts: [
                { arg: 'req', type: 'object', http: { source: 'req' } },
                { arg: 'res', type: 'object', http: { source: 'res' } },
            ],
            returns: { arg: 'documents', type: 'object', root: true },
            http: { verb: 'get' },
        })

        DocumentModel.remoteMethod('updateDocument', {
            description: 'Updates a document',
            accepts: [
                { arg: 'req', type: 'object', http: { source: 'req' } },
                { arg: 'res', type: 'object', http: { source: 'res' } },
                { arg: 'body', type: 'object', http: { source: 'body' } },
            ],
            returns: { arg: 'fileObject', type: 'object', root: true },
            http: { verb: 'put' },
        })

        Document.remoteMethod('deleteDocument', {
            description: 'Deletes a Document by Id',
            accepts: [
                { arg: 'req', type: 'object', http: { source: 'req' } },
                { arg: 'res', type: 'object', http: { source: 'res' } },
                { arg: 'body', type: 'object', http: { source: 'body' } },
            ],
            returns: { arg: 'fileObject', type: 'object', root: true },
            http: { verb: 'delete' },
        })

        Document.remoteMethod('filterDocuments', {
            description: 'Get all documents filtered by the params',
            accepts: [
                { arg: 'req', type: 'object', http: { source: 'req' } },
                { arg: 'res', type: 'object', http: { source: 'res' } },
            ],
            returns: { arg: 'documents', type: 'object', root: true },
            http: { verb: 'get' },
        })

        Document.remoteMethod('getDocumentCount', {
            description:
                'Get the number of documents the user is allowed to see',
            accepts: [
                { arg: 'req', type: 'object', http: { source: 'req' } },
                { arg: 'res', type: 'object', http: { source: 'res' } },
            ],
            returns: { arg: 'number', type: 'number', root: true },
            http: { verb: 'get' },
        })

        return DocumentModel
    }

    Document.setup()
}

function createFilterAuthPromise(AccessToken, User, token) {
    return new Promise((resolve, reject) => {
        if (token) {
            //Validation of usertoken
            return AccessToken.findOne(
                { where: { id: token } },
                (err, tokenEntry) => {
                    if (err) reject(err)
                    else if (!tokenEntry) {
                        //no token found
                        reject(
                            errorHandler.createError(
                                'permissions needed to upload',
                                401,
                                'UPLOAD_DENIED',
                            ),
                        )
                    } else {
                        //Validation of token expiry date
                        var date = new Date(tokenEntry.created)
                        date.setSeconds(date.getSeconds() + tokenEntry.ttl)
                        if (date >= new Date()) {
                            //token is valid
                            return User.findOne(
                                { where: { id: tokenEntry.userId } },
                                (err, userEntry) => {
                                    if (err) reject(err)
                                    else if (!userEntry) {
                                        reject(
                                            errorHandler.createError(
                                                'user not found',
                                                401,
                                                'UPLOAD_DENIED',
                                            ),
                                        )
                                    } else {
                                        //validation of studentAccount if user is student
                                        if (userEntry.userRole == 'student') {
                                            userEntry.authWithStudentAccount(
                                                {
                                                    where: {
                                                        expiryDate: {
                                                            gt: new Date(),
                                                        },
                                                    },
                                                },
                                                (err, studentAccounts) => {
                                                    if (err) reject(err)
                                                    else if (
                                                        studentAccounts.length ==
                                                        0
                                                    ) {
                                                        reject(
                                                            errorHandler.createError(
                                                                'no valid studentAccount was found',
                                                                400,
                                                                'NO_VALID_ACCOUNT',
                                                            ),
                                                        )
                                                    } else {
                                                        resolve(
                                                            getRoleNumber(
                                                                userEntry.userRole,
                                                            ),
                                                        )
                                                    }
                                                },
                                            )
                                        } else {
                                            //user is no student
                                            resolve(
                                                getRoleNumber(
                                                    userEntry.userRole,
                                                ),
                                            )
                                        }
                                    }
                                },
                            )
                        } else {
                            resolve(0)
                        }
                    }
                },
            )
        } else {
            resolve(0)
        }
    })
}

function deleteDocumentEntry(Document, documentEntry) {
    Document.destroyAll(documentEntry, (err, info) => {
        if (err) {
        } else {
        }
    })
}

function deleteReleaseReqeuest(ReleaseRequest, documentId) {
    ReleaseRequest.destroyAll({ documentId: documentId }, (err, info) => {
        if (err) {
        } else {
        }
    })
}

function deleteFolder(documentId) {
    deleteHandler.deleteFolder(documentId)
}

function removeFile(Container, documentId, filename, path) {
    fs_extra.remove(path, (error, reply) => {
        if (error) {
            return new Error(error)
        } else {
            deleteFolder(documentId)
        }
    })
}

function removeUpdateFile(Container, documentId, filename) {
    Container.removeFile(documentId, filename, (error, reply) => {
        if (error) {
            return new Error(error)
        }
    })
}

function getRoleNumber(roleString) {
    switch (roleString) {
        case 'student': {
            return 1
        }
        case 'docent': {
            return 2
        }
        case 'admin': {
            return 3
        }
        default: {
            return 0
        }
    }
}

function addSearchwords(document, searchwords, cb, SearchWords) {
    searchwords.forEach((searchWord) => {
        SearchWords.findOne(
            { where: { word: searchWord.toLowerCase() } },
            (err, result) => {
                if (err) {
                    cb(err)
                    return cb.promise
                } else if (!result) {
                    createSearchWord(document, searchWord.toLowerCase())
                } else {
                    addSearchWordToDocument(result, document)
                }
            },
        )
    })
}

function createSearchWord(document, searchWord) {
    document.searchWords.create({ word: searchWord }, (err, response) => {})
}

function addSearchWordToDocument(result, document) {
    document.searchWords.add(result, (err) => {
        if (err) {
        }
    })
}

function checkBucketFolder(storageName, storageRoot) {
    try {
        if (storageName === 'storage') {
            const path = `${storageRoot}${BUCKET}/`

            if (!fs.existsSync(path)) {
                fs.mkdirSync(path)
            }
        }
    } catch (error) {}
}

function checkForCorrectContentType(req) {
    if (!String(req.headers['content-type']).includes('multipart/form-data')) {
        return errorHandler.ErrorLogging(
            'wrong conent-type',
            400,
            'INVALID_CONTENTTYPE',
        )
    }
}

function createFieldsPromise(req, form) {
    return new Promise((resolve, reject) => {
        form.parse(req, (error, fields, files) => {
            if (error) reject(error)
            else if (
                files.file === undefined ||
                files.file.type != 'application/pdf'
            )
                reject(
                    errorHandler.createError(
                        'parameter file (pdf) required',
                        400,
                        'FILE_REQUIRED',
                    ),
                )
            else if (!fields.title || typeof fields.title != 'string')
                reject(
                    errorHandler.createError(
                        'parameter title required',
                        400,
                        'TITLE_REQUIRED',
                    ),
                )
            else if (!fields.language || typeof fields.language != 'string')
                reject(
                    errorHandler.createError(
                        'parameter language required',
                        400,
                        'LANGUAGE_REQUIRED',
                    ),
                )
            else if (!fields.type || typeof fields.type != 'string')
                reject(
                    errorHandler.createError(
                        'parameter type required',
                        400,
                        'TYPE_REQUIRED',
                    ),
                )
            else if (!fields.year)
                reject(
                    errorHandler.createError(
                        'parameter year required',
                        400,
                        'YEAR_REQUIRED',
                    ),
                )
            else if (!fields.course || typeof fields.course != 'string')
                reject(
                    errorHandler.createError(
                        'parameter course required',
                        400,
                        'COURSE_REQUIRED',
                    ),
                )
            else if (!fields.docent || typeof fields.docent != 'string')
                reject(
                    errorHandler.createError(
                        'parameter docent required',
                        400,
                        'DOCENT_REQUIRED',
                    ),
                )
            else if (!fields.company || typeof fields.company != 'string')
                reject(
                    errorHandler.createError(
                        'parameter company required',
                        400,
                        'COMPANY_REQUIRED',
                    ),
                )
            else if (!fields.restricted)
                reject(
                    errorHandler.createError(
                        'parameter restricted required',
                        400,
                        'RESTRICTED_REQUIRED',
                    ),
                )
            else if (!fields.abstract || typeof fields.abstract != 'string')
                reject(
                    errorHandler.createError(
                        'parameter abstract required',
                        400,
                        'ABSTRACT_REQUIRED',
                    ),
                )
            else if (!fields.privacyState)
                reject(
                    errorHandler.createError(
                        'parameter privacyState required',
                        400,
                        'PRIVACYSTATE_REQUIRED',
                    ),
                )
            else if (!fields.searchWords)
                reject(
                    errorHandler.createError(
                        'parameter searchWords required',
                        400,
                        'SEARCHWORDS_REQUIRED',
                    ),
                )
            else if (!fields.student)
                reject(
                    errorHandler.createError(
                        'parameter student required',
                        400,
                        'STUDENT_REQUIRED',
                    ),
                )
            else {
                resolve([fields, files])
            }
        })
    })
}

function createUpdateFieldsPromise(req, form) {
    return new Promise((resolve, reject) => {
        form.parse(req, (error, fields, files) => {
            if (error) reject(error)
            //Validation of all parameters
            else if (!fields.id)
                reject(
                    errorHandler.createError(
                        'parameter documentId required',
                        400,
                        'DOCUMENTID_REQUIRED',
                    ),
                )
            else if (!fields.title || typeof fields.title != 'string')
                reject(
                    errorHandler.createError(
                        'parameter title required',
                        400,
                        'TITLE_REQUIRED',
                    ),
                )
            else if (!fields.language || typeof fields.language != 'string')
                reject(
                    errorHandler.createError(
                        'parameter language required',
                        400,
                        'LANGUAGE_REQUIRED',
                    ),
                )
            else if (!fields.type || typeof fields.type != 'string')
                reject(
                    errorHandler.createError(
                        'parameter type required',
                        400,
                        'TYPE_REQUIRED',
                    ),
                )
            else if (!fields.year)
                reject(
                    errorHandler.createError(
                        'parameter year required',
                        400,
                        'YEAR_REQUIRED',
                    ),
                )
            else if (!fields.course || typeof fields.course != 'string')
                reject(
                    errorHandler.createError(
                        'parameter course required',
                        400,
                        'COURSE_REQUIRED',
                    ),
                )
            else if (!fields.docent || typeof fields.docent != 'string')
                reject(
                    errorHandler.createError(
                        'parameter docent required',
                        400,
                        'DOCENT_REQUIRED',
                    ),
                )
            else if (!fields.company || typeof fields.company != 'string')
                reject(
                    errorHandler.createError(
                        'parameter company required',
                        400,
                        'COMPANY_REQUIRED',
                    ),
                )
            else if (!fields.restricted)
                reject(
                    errorHandler.createError(
                        'parameter restricted required',
                        400,
                        'RESTRICTED_REQUIRED',
                    ),
                )
            else if (!fields.abstract || typeof fields.abstract != 'string')
                reject(
                    errorHandler.createError(
                        'parameter abstract required',
                        400,
                        'ABSTRACT_REQUIRED',
                    ),
                )
            else if (!fields.privacyState)
                reject(
                    errorHandler.createError(
                        'parameter privacyState required',
                        400,
                        'PRIVACYSTATE_REQUIRED',
                    ),
                )
            else if (!fields.searchWords)
                reject(
                    errorHandler.createError(
                        'parameter searchWords required',
                        400,
                        'SEARCHWORDS_REQUIRED',
                    ),
                )
            else if (!fields.student)
                reject(
                    errorHandler.createError(
                        'parameter student required',
                        400,
                        'STUDENT_REQUIRED',
                    ),
                )
            else if (
                files.file === undefined ||
                files.file.type != 'application/pdf'
            ) {
                resolve([fields, null, false])
            } else {
                resolve([fields, files, true])
            }
        })
    })
}

function createDeletePromise(req, form) {
    return new Promise((resolve, reject) => {
        form.parse(req, (error, fields) => {
            if (error) reject(error)
            else if (!fields.id)
                reject(
                    errorHandler.createError(
                        'parameter documentId required',
                        400,
                        'DOCUMENTID_REQUIRED',
                    ),
                )
            else {
                resolve([fields])
            }
        })
    })
}

function createAuthPromise(AccessToken, User, token) {
    return new Promise((resolve, reject) => {
        if (token) {
            //Validation of usertoken
            return AccessToken.findOne(
                { where: { id: token } },
                (err, tokenEntry) => {
                    if (err) reject(err)
                    else if (!tokenEntry) {
                        //no token found
                        reject(
                            errorHandler.createError(
                                'permissions needed to upload',
                                401,
                                'UPLOAD_DENIED',
                            ),
                        )
                    } else {
                        //Validation of token expiry date
                        var date = new Date(tokenEntry.created)
                        date.setSeconds(date.getSeconds() + tokenEntry.ttl)
                        if (date >= new Date()) {
                            //token is valid
                            return User.findOne(
                                { where: { id: tokenEntry.userId } },
                                (err, userEntry) => {
                                    if (err) reject(err)
                                    else if (!userEntry) {
                                        reject(
                                            errorHandler.createError(
                                                'user not found',
                                                401,
                                                'UPLOAD_DENIED',
                                            ),
                                        )
                                    } else {
                                        //validation of studentAccount if user is student
                                        if (userEntry.userRole == 'student') {
                                            userEntry.authWithStudentAccount(
                                                {
                                                    where: {
                                                        expiryDate: {
                                                            gt: new Date(),
                                                        },
                                                    },
                                                },
                                                (err, studentAccounts) => {
                                                    if (err) reject(err)
                                                    else if (
                                                        studentAccounts.length ==
                                                        0
                                                    ) {
                                                        reject(
                                                            errorHandler.createError(
                                                                'no valid studentAccount was found',
                                                                400,
                                                                'NO_VALID_ACCOUNT',
                                                            ),
                                                        )
                                                    } else {
                                                        resolve(userEntry)
                                                    }
                                                },
                                            )
                                        } else {
                                            //user is no student
                                            resolve(userEntry)
                                        }
                                    }
                                },
                            )
                        } else {
                            reject(
                                errorHandler.createError(
                                    'token is expired',
                                    401,
                                    'TOKEN_EXPIRED',
                                ),
                            )
                        }
                    }
                },
            )
        } else {
            reject(
                errorHandler.createError(
                    'permissions needed to upload',
                    401,
                    'UPLOAD_DENIED',
                ),
            )
        }
    })
}

function createDocumentEntryPromise(authPromise, fieldsPromise) {
    return new Promise((resolve, reject) => {
        //run the code only if both authPromise and fieldsPromise are resolving
        Promise.all([authPromise, fieldsPromise])
            .then(([userEntry, [fields, files]]) => {
                var documentEntry = {
                    title: fields.title,
                    language: fields.language,
                    type: fields.type,
                    year: Number(fields.year),
                    course: fields.course,
                    docent: fields.docent,
                    company: fields.company,
                    restricted: fields.restricted == 'true',
                    abstract: fields.abstract,
                    privacyState: Number(fields.privacyState),
                    fileName: files.file.name,
                    userId: userEntry.id,
                    student: fields.student,
                }
                resolve([documentEntry, JSON.parse(fields.searchWords)])
            })
            .catch((error) => {
                reject(error)
            })
    })
}

function createUpdateDocumentEntryPromise(authPromise, fieldsPromise) {
    return new Promise((resolve, reject) => {
        //run the code only if both authPromise and fieldsPromise are resolving
        Promise.all([authPromise, fieldsPromise])
            .then(([userEntry, [fields, files, getFileUpdated]]) => {
                var documentEntry = {
                    id: fields.id,
                    title: fields.title,
                    language: fields.language,
                    type: fields.type,
                    year: Number(fields.year),
                    course: fields.course,
                    docent: fields.docent,
                    company: fields.company,
                    restricted: fields.restricted == 'true',
                    abstract: fields.abstract,
                    privacyState: Number(fields.privacyState),
                    userId: userEntry.id,
                    student: fields.student,
                    ...(getFileUpdated && { fileName: files.file.name }),
                }
                if (getFileUpdated == true) {
                    resolve([
                        documentEntry,
                        JSON.parse(fields.searchWords),
                        true,
                    ])
                } else {
                    resolve([
                        documentEntry,
                        JSON.parse(fields.searchWords),
                        false,
                    ])
                }
            })
            .catch((error) => {
                reject(error)
            })
    })
}

function createFilterEntryPromsie(fieldsPromise) {
    return new Promise((resolve, reject) => {
        Promise.all([fieldsPromise])
            .then(([[fields]]) => {
                var filterOptions = {
                    language: fields.language,
                    year: fields.year,
                    type: fields.type,
                }
                resolve([filterOptions])
            })
            .catch((error) => {
                reject(error)
            })
    })
}

function createDeleteDocumentEntryPromise(fieldsPromise) {
    return new Promise((resolve, reject) => {
        //run the code only if both authPromise and fieldsPromise are resolving
        Promise.all([fieldsPromise])
            .then(([[fields]]) => {
                var documentEntry = {
                    id: fields.id,
                }
                resolve([documentEntry])
            })
            .catch((error) => {
                reject(error)
            })
    })
}

function createFilePromise(req, Container, res) {
    return new Promise((resolve, reject) => {
        Container.upload(req, res, { container: BUCKET }, (error, fileObj) => {
            if (error) {
                return reject(error)
            }
            resolve(fileObj)
        })
    })
}

function createUpdateFilePromise(req, Container, res, cb) {
    return new Promise((resolve, reject) => {
        Container.upload(req, res, { container: BUCKET }, (error, fileObj) => {
            if (error) {
                if (error.toString().includes('No file content uploaded')) {
                    resolve(null)
                } else {
                    cb(error)
                    return cb.promise
                }
            }
            resolve(fileObj)
        })
    })
}

function deleteDocumentEntryAndFile(
    Document,
    document,
    Container,
    documentEntry,
    cb,
) {
    Document.destroyById(document.id)
    //Franzi
    Container.removeFile(BUCKET, documentEntry.fileName, (err, result) => {
        if (err) {
            cb(err)
            return cb.promise
        } else
            return errorHandler.ErrorLogging(
                'no valid studentAccount was found',
                400,
                'NO_VALID_ACCOUNT',
            )
    })
}
