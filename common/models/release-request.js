'use strict'

const utils = require('loopback/lib/utils')
const errorHandler = require('../../handler/errorHandler')

module.exports = function (Releaserequest) {
    Releaserequest.answerRequest = (data, req, res, cb) => {
        const AccessToken = Releaserequest.app.models.AccessToken
        const User = Releaserequest.app.models.userLogin
        cb = cb || utils.createPromiseCallback()

        const authPromise = new Promise((resolve, reject) => {
            if (req.headers.accesstoken) {
                //Validation of usertoken
                return AccessToken.findOne(
                    { where: { id: req.headers.accesstoken } },
                    (err, tokenEntry) => {
                        if (err) reject(err)
                        else if (!tokenEntry) {
                            //no token found -> normal user
                            reject(
                                errorHandler.createError(
                                    'permissions needed to post',
                                    401,
                                    'REQUEST_DENIED',
                                ),
                            )
                        } else {
                            //Validation of token expiry date
                            var date = new Date(tokenEntry.created)
                            date.setSeconds(date.getSeconds() + tokenEntry.ttl)
                            if (date >= new Date()) {
                                //token is valid
                                return User.findOne(
                                    { where: { id: tokenEntry.userId } },
                                    (err, userEntry) => {
                                        if (err) reject(err)
                                        else if (!userEntry) {
                                            //no user was found
                                            reject(
                                                errorHandler.createError(
                                                    'permissions needed to post',
                                                    401,
                                                    'REQUEST_DENIED',
                                                ),
                                            )
                                        } else {
                                            //only accept userRole docent
                                            if (
                                                userEntry.userRole == 'docent'
                                            ) {
                                                resolve(userEntry)
                                            } else {
                                                reject(
                                                    errorHandler.createError(
                                                        'permissions needed to post',
                                                        401,
                                                        'REQUEST_DENIED',
                                                    ),
                                                )
                                            }
                                        }
                                    },
                                )
                            } else {
                                reject(
                                    errorHandler.createError(
                                        'permissions needed to post',
                                        401,
                                        'REQUEST_DENIED',
                                    ),
                                )
                            }
                        }
                    },
                )
            } else {
                reject(
                    errorHandler.createError(
                        'permissions needed to post',
                        401,
                        'REQUEST_DENIED',
                    ),
                )
            }
        })
        const parameterPromise = new Promise((resolve, reject) => {
            if (!data.requestId || typeof data.requestId != 'string')
                reject(
                    errorHandler.createError(
                        'parameter requestId required',
                        400,
                        'REQUESTID_REQUIRED',
                    ),
                )
            else if (data.isDocumentAccepted == undefined)
                reject(
                    errorHandler.createError(
                        'parameter isDocumentAccepted required',
                        400,
                        'ISDOCUMENTACCEPTED_REQUIRED',
                    ),
                )
            else {
                const releaseRequestEntry = {
                    releaseState: Boolean(data.isDocumentAccepted) ? 1 : 2,
                    ...(data.message && { message: data.message }),
                }

                resolve(releaseRequestEntry)
            }
        })

        Promise.all([authPromise, parameterPromise])
            .then(([userEntry, releaseRequestEntry]) => {
                Releaserequest.findById(data.requestId, (err, result) => {
                    if (err) {
                        cb(err)
                        return cb.promise
                    }
                    result.updateAttributes(
                        releaseRequestEntry,
                        (err, updatedReleaseRequest) => {
                            if (err) {
                                cb(err)
                                return cb.promise
                            }
                            cb(null, updatedReleaseRequest)
                            return cb.promise
                        },
                    )
                })
            })
            .catch((error) => {
                cb(error)
                return cb.promise
            })
    }

    Releaserequest.unfullifiedRequests = (req, res, cb) => {
        const AccessToken = Releaserequest.app.models.AccessToken
        const User = Releaserequest.app.models.userLogin
        cb = cb || utils.createPromiseCallback()

        const authPromise = new Promise((resolve, reject) => {
            if (req.headers.accesstoken) {
                //Validation of usertoken
                return AccessToken.findOne(
                    { where: { id: req.headers.accesstoken } },
                    (err, tokenEntry) => {
                        if (err) reject(err)
                        else if (!tokenEntry) {
                            //no token found -> normal user
                            reject(
                                errorHandler.createError(
                                    'permissions needed to get',
                                    401,
                                    'REQUEST_DENIED',
                                ),
                            )
                        } else {
                            //Validation of token expiry date
                            var date = new Date(tokenEntry.created)
                            date.setSeconds(date.getSeconds() + tokenEntry.ttl)
                            if (date >= new Date()) {
                                //token is valid
                                return User.findOne(
                                    { where: { id: tokenEntry.userId } },
                                    (err, userEntry) => {
                                        if (err) reject(err)
                                        else if (!userEntry) {
                                            //no user was found
                                            reject(
                                                errorHandler.createError(
                                                    'permissions needed to get',
                                                    401,
                                                    'REQUEST_DENIED',
                                                ),
                                            )
                                        } else {
                                            //only accept userRole docent
                                            if (
                                                userEntry.userRole == 'docent'
                                            ) {
                                                resolve(userEntry)
                                            } else {
                                                reject(
                                                    errorHandler.createError(
                                                        'permissions needed to get',
                                                        401,
                                                        'REQUEST_DENIED',
                                                    ),
                                                )
                                            }
                                        }
                                    },
                                )
                            } else {
                                reject(
                                    errorHandler.createError(
                                        'permissions needed to get',
                                        401,
                                        'REQUEST_DENIED',
                                    ),
                                )
                            }
                        }
                    },
                )
            } else {
                reject(
                    errorHandler.createError(
                        'permissions needed to get',
                        401,
                        'REQUEST_DENIED',
                    ),
                )
            }
        })

        authPromise
            .then((userEntry) => {
                //get all studentAccounts (also the expired) with their unfullified releaseRequests and beloying documents
                userEntry.managedStudentAccount(
                    {
                        include: {
                            relation: 'releaseRequests',
                            scope: {
                                where: { releaseState: 0 },
                                include: { relation: 'document' },
                            },
                            where: { disabled: { neq: true } },
                        },
                    },
                    (err, studentAccountEntry) => {
                        cb(null, studentAccountEntry)
                        return cb.promise
                    },
                )
            })
            .catch((err) => {
                cb(err)
                return cb.promise
            })
    }

    Releaserequest.setup = function () {
        // We need to call the base class's setup method
        Releaserequest.base.setup.call(this)
        var ReleaserequestModel = this

        ReleaserequestModel.remoteMethod('answerRequest', {
            description: 'Answer a open request',
            accepts: [
                {
                    arg: 'data',
                    type: 'object',
                    required: true,
                    http: { source: 'body' },
                },
                { arg: 'req', type: 'object', http: { source: 'req' } },
                { arg: 'res', type: 'object', http: { source: 'res' } },
            ],
            returns: { arg: 'fileObject', type: 'object', root: true },
            http: { verb: 'post' },
        })

        ReleaserequestModel.remoteMethod('unfullifiedRequests', {
            description: 'Get all open requests',
            accepts: [
                { arg: 'req', type: 'object', http: { source: 'req' } },
                { arg: 'res', type: 'object', http: { source: 'res' } },
            ],
            returns: { arg: 'fileObject', type: 'object', root: true },
            http: { verb: 'get' },
        })

        return ReleaserequestModel
    }

    Releaserequest.setup()
}
